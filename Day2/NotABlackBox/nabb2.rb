#!/usr/bin/env ruby -w
def square(x)
  x * x
end

def f(x)
  result = 42 + 5.0 * (rand - 0.5)
  result += 20 if x[6] < 40 && x[6] > 15
  result -= 10 if x[11] > 60
  result
end

NREPS = (ARGV.shift || 5).to_i
loop do
  line = gets || break
  inputs = line.strip.split(/[\s,;]+/).map(&:to_f)
  if inputs.length != 20
    STDERR.puts 'Must supply 20 inputs'
    break
  end
  if inputs.any? { |x| x < 0 || x > 100 }
    STDERR.puts 'Values must be between 0 and 100'
    break
  end
  data = [0] + inputs # offset so array index is 1-based
  # NREPS.times { puts inputs.join(',') + ',' + f(data).to_s }
  NREPS.times { puts((inputs + [f(data)]).join(',')) }
end
