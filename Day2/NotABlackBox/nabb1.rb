#!/usr/bin/env ruby -w
def square(x)
  x * x
end

def f(x)
  42 + 0.7 * x[3] + -0.015 * square(x[3] - 50) +
    -0.9 * x[13] +
    0.04 * (x[7] - 50) * (x[9] - 50) +
    -0.06 * (x[15] - 50) * (x[1] - 50) +
    5.0 * (rand - 0.5)
end

NREPS = (ARGV.shift || 5).to_i
loop do
  line = gets || break
  inputs = line.strip.split(/[\s,;]+/).map(&:to_f)
  if inputs.length != 20
    STDERR.puts 'Must supply 20 inputs'
    break
  end
  if inputs.any? { |x| x < 0 || x > 100 }
    STDERR.puts 'Values must be between 0 and 100'
    break
  end
  data = [0] + inputs # offset so array index is 1-based
  # NREPS.times { puts inputs.join(',') + ',' + f(data).to_s }
  NREPS.times { puts((inputs + [f(data)]).join(',')) }
end
