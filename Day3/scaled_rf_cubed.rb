#!/usr/bin/env ruby -w

require 'rubygems' if RUBY_VERSION =~ /^1\.8/
begin
  require 'fwt'
rescue LoadError
  STDERR.print "\nALERT:\tFWT is not installed!"
  STDERR.puts "\n\tCorrect this by running the command \"gem install fwt\"."
  STDERR.puts "\t(This might require administrative privileges.)"
  STDERR.puts
  exit
end

# Generate a Resolution V Fractional Factorial design for the specified
# number of factors.  The design is guaranteed to yield unconfounded
# interactions for all pairs of factors.  The design uses standardized
# notation, i.e., -1 represents a low setting and 1 represents a high
# setting for each factor.
#
# *Arguments*::
#   - +number_of_factors+ -> the number of factors in your design. Limit is 120.
# *Returns*::
#   - a two-dimensional array specifying the design, where each column
#     corresponds to a factor and each row is a design point.
#
# Author:: Paul J Sanchez (mailto:pjs@alum.mit.edu)
# Copyright:: Copyright (c) Paul J Sanchez
# License:: LGPL
#
def makeDesign(number_of_factors)
  index = [1, 2, 4, 8, 15, 16, 32, 51, 64, 85, 106, 128, 150, 171,
      219, 237, 247, 256, 279, 297, 455, 512, 537, 557, 594, 643, 803,
      863, 998, 1024, 1051, 1070, 1112, 1169, 1333, 1345, 1620, 1866,
      2048, 2076, 2085, 2185, 2372, 2456, 2618, 2800, 2873, 3127, 3284,
      3483, 3557, 3763, 4096, 4125, 4135, 4174, 4435, 4459, 4469, 4497,
      4752, 5255, 5732, 5804, 5915, 6100, 6369, 6907, 7069, 8192, 8263,
      8351, 8422, 8458, 8571, 8750, 8858, 9124, 9314, 9500, 10026,
      10455, 10556, 11778, 11885, 11984, 13548, 14007, 14514, 14965,
      15125, 15554, 16384, 16457, 16517, 16609, 16771, 16853, 17022,
      17453, 17891, 18073, 18562, 18980, 19030, 19932, 20075, 20745,
      21544, 22633, 23200, 24167, 25700, 26360, 26591, 26776, 28443,
      28905, 29577, 32705]
  power = [1, 2, 4, 8, 16, 16, 32, 64, 64, 128, 128, 128, 256, 256,
      256, 256, 256, 256, 512, 512, 512, 512, 1024, 1024, 1024, 1024, 1024,
      1024, 1024, 1024, 2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
      2048, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096,
      4096, 4096, 4096, 4096, 8192, 8192, 8192, 8192, 8192, 8192, 8192,
      8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 16384,
      16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384,
      16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384,
      16384, 16384, 16384, 32768, 32768, 32768, 32768, 32768, 32768,
      32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768,
      32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768,
      32768, 32768, 32768, 32768]

  design = Array.new(number_of_factors)
  design.each_index do |i|
      design[i] = Array.new(power[number_of_factors], 0)
      design[i][index[i]] = 1
      design[i].hadamard
    end
  design.transpose
end

def print_directions  # :nodoc:
  STDERR.puts "\nRun-time options:"
  STDERR.puts "\t--design-only (or -d): print the design without labeling"
  STDERR.puts "\t--csv (or -c): print the design as comma separated"
  STDERR.puts "\t--center (or -a): add global center pt to check linearity"
  STDERR.puts "\t--star (or -s): augment with Central Composite Design pts"
  STDERR.puts "\nUse command-line args to specify either:"
  STDERR.puts "\t1) the number of factors for the design; or"
  STDERR.puts "\t2) low and hi values for all factors."
  STDERR.puts
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 0
    print_directions
    exit
  end

  begin
    labels = true
    csv = false
    scaled = false
    center = false
    star = false
    fmt_size = 5
    fmt_type = 'd'

    while ARGV[0] =~ /^-\D/
      arg = ARGV.shift
      case arg
      when '-c', '--csv'
        csv = true
      when '-d', '--design-only'
        labels = false
      when '-a', '--center'
        center = true
      when '-s', '--star'
        star = true
      else
        fail 'Unknown option \"#{arg}\"'
      end
    end

    if ARGV.length == 0
      fail 'Must specify a design size'
    elsif ARGV.length == 1
      n = ARGV.shift.to_i
    elsif (ARGV.length & 1).nonzero?     # Odd number of args remaining!
      fail 'Number of levels is odd: low/hi values must occur in pairs'
    else
      n = ARGV.length / 2
      scaled = true
      fmt_type = 's'
      fmt_size = [(ARGV.map(&:length)).max + 1, fmt_size].max
    end

    my_design = makeDesign(n)
    my_design += [Array.new(n, 0)] if center

    if star
      star_design = Array.new(2*n)
      star_design.each_index {|row| star_design[row] = Array.new(n, 0)}
      n.times do |col|
        row = 2 * col
        star_design[row][col] = -1
        star_design[row + 1][col] = 1
      end
      my_design += star_design
    end

    if scaled
      temp = my_design.transpose
      temp.each_with_index do |factor, idx|
        factor.map! do |value|
          case value
          when -1
            ARGV[2 * idx + 1]
          when 1
            ARGV[2 * idx]
          when 0
            begin
              0.5 * (Float(ARGV[2 * idx + 1]) + Float(ARGV[2 * idx]))
            rescue
              raise 'All factors must be numeric for augmented designs.'
            end
          end
        end
      end
      my_design = temp.transpose
    end

    if labels
      if csv
        print ' DP#'
        n.times { |i| print ",X#{i + 1}" }
      else
        print '  DP#'
        n.times { |i| printf " %#{fmt_size}s", "X#{i + 1}" }
      end
      puts
    end

    my_design.each_with_index do |design_point, row_number|
      if csv
        printf('%d,', row_number + 1) if labels
        puts design_point.join(',')
      else
        printf('%5d', row_number + 1) if labels
        design_point.each { |value| printf " %#{fmt_size}#{fmt_type}", value }
        puts
      end
    end
  rescue Exception => e
    STDERR.print "\n", e.message, "\n"
    print_directions
  end
end
