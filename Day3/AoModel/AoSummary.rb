#!/usr/bin/env ruby

if RUBY_VERSION =~ /^1\.8/
  require 'rubygems'
end

begin
  require 'simplekit'
  require 'quickstats'
rescue LoadError => e
  missing = e.message.split[-1]
  STDERR.puts "\nERROR:  " + e.message
  STDERR.puts "\n\tRun the command \"gem install #{missing}\" to fix this."
  STDERR.puts "\t(This may require administrative privileges on Windows or"
  STDERR.puts "\tuse of the \"sudo\" command on OS X or linux.)"
  STDERR.puts
  exit
end

# Demonstration model of Operational Availability (Ao).

class AoModel
  include SimpleKit

  # model state
  attr_reader  :numAvailableJeeps,
    :numAvailableMechanics,
    :maintenanceQLength,
    :breakdownQLength

  # model parameters
  attr_reader  :maxJeeps,
    :maxMaintainers,
    :breakdownRate,
    :maintenanceCycleInDays,
    :repairRate,
    :haltTime

  # Exponential random variate generator with specified rate.
  def exponential(rate)
    -Math.log(rand) / rate
  end

  # the actual model implementation...

  # Constructor initializes the model parameters.
  def initialize(maxJeeps, maxMaintainers, breakdownRate,
                 maintenanceCycleInDays, meanRepairTime, haltTime)
    @maxJeeps = maxJeeps
    @maxMaintainers = maxMaintainers
    @breakdownRate = breakdownRate
    @maintenanceCycleInDays = maintenanceCycleInDays
    @repairRate = 1.0 / meanRepairTime
    @haltTime = haltTime
    @stats = QuickStats.new
    @data = []
  end

  # init method kickstarts a simplekit model.  State variables are
  # set to initial values, and some preliminary events get scheduled
  def init
    @numAvailableJeeps = @maxJeeps
    @numAvailableMechanics = @maxMaintainers
    @maintenanceQLength = 0
    @breakdownQLength = 0
    @numAvailableJeeps.times do |i|
      breakdownTime = exponential(@breakdownRate)
      if (breakdownTime <= @maintenanceCycleInDays)
        schedule(:breakdown, breakdownTime)
      else
        schedule(:maintenance, @maintenanceCycleInDays)
      end
    end
    schedule(:finalReport, @haltTime)
    schedule(:dailyStats, 0.0)
  end

  # Event methods follow...

  def dailyStats
    if model_time < @haltTime && model_time >= 800
      @data << @numAvailableJeeps
      @stats.new_obs @numAvailableJeeps
    end
    schedule(:dailyStats, 8.0)
  end

  def finalReport
    @data.sort!
    lower_decile = (0.1 * @data.length).round.to_i
    upper_decile = (0.9 * @data.length).to_i
    printf "%f,%f,%f,%f,", @stats.avg, @stats.std_dev, @stats.min, @stats.max
    printf "%f,%f,%d\n", @data[lower_decile], @data[upper_decile], @stats.n
    schedule(:halt, 0)
  end

  def breakdown
    @breakdownQLength += 1
    @numAvailableJeeps -= 1
    schedule(:beginBreakdownService, 0.0) if (@numAvailableMechanics > 0)
  end

  def maintenance
    @maintenanceQLength += 1
    @numAvailableJeeps -= 1
    schedule(:beginMaintenanceService, 0.0) if (@numAvailableMechanics > 0)
  end

  def beginMaintenanceService
    @maintenanceQLength -= 1
    @numAvailableMechanics -= 1
    if (rand <= 0.95)
      schedule(:endService, (6.5 - rand) / 8.0)
    else
      schedule(:endService, exponential(@repairRate / 4.0))
    end
  end

  def beginBreakdownService
    @breakdownQLength -= 1
    @numAvailableMechanics -= 1
    if (rand <= 0.8)
      schedule(:endService, exponential(@repairRate))
    else
      schedule(:endService, exponential(@repairRate / 4.0))
    end
  end

  def endService
    @numAvailableMechanics += 1
    @numAvailableJeeps += 1
    if (@maintenanceQLength > 0)
      schedule(:beginMaintenanceService, 0.0)
    else
      schedule(:beginBreakdownService, 0.0) if (@breakdownQLength > 0)
    end
    breakdownTime = exponential(@breakdownRate)
    if (breakdownTime <= @maintenanceCycleInDays)
      schedule(:breakdown, breakdownTime)
    else
      schedule(:maintenance, @maintenanceCycleInDays)
    end
  end

end

# Run model based on command-line arguments...
if (ARGV.length != 6)
  STDERR.puts "\nMust supply six command-line arguments:\n"
  STDERR.puts "\tInitial Stock level (int)"
  STDERR.puts "\t#Maintenance personnel (int)"
  STDERR.puts "\tNormal breakdown rate (double)"
  STDERR.puts "\tMaintenance cycle length (int)"
  STDERR.puts "\tMean repair time (double)"
  STDERR.puts "\tNumber of days to run (int)"
  STDERR.puts "\nExample: ruby #{File.basename($0)} 50 2 0.01 90 3.0 50\n"
else
  maxJeeps = ARGV[0].to_i
  maxMaintainers = ARGV[1].to_i
  breakdownRate = ARGV[2].to_f
  maintenanceCycleInDays = ARGV[3].to_i
  meanRepairTime = ARGV[4].to_f
  haltTimeInDays = ARGV[5].to_i
  if haltTimeInDays <= 100
    STDERR.puts "\n\tModel truncates first 100 days, specify a longer run."
    STDERR.puts
  else
    STDOUT.print "initial_stock,#maintainers,breakdown_rate,repair_cycle,"
    STDOUT.print "mean_repair_time,run_length,mean_availability,"
    STDOUT.print "std_dev,min,max,lower_decile,upper_decile,n\n"
    STDOUT.print ARGV.join(",") + ","
    AoModel.new(maxJeeps, maxMaintainers, breakdownRate,
              maintenanceCycleInDays, meanRepairTime, 8.0 * haltTimeInDays).run
  end
end
